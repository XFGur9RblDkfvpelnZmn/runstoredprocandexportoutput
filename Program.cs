﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;

namespace RunStoredProcAndExportOutput
{
    class Program
    {
        static void Main(string[] args)
        {
            

            string connectionString = Properties.Settings.Default.connectionString[Convert.ToInt32(args[0])];
            string storedProc = Properties.Settings.Default.storedProc[Convert.ToInt32(args[0])];
            string parameters = Properties.Settings.Default.parameters[Convert.ToInt32(args[0])];
            string outputFile = Properties.Settings.Default.outputFile[Convert.ToInt32(args[0])];
            string outputMap = Properties.Settings.Default.outputMap[Convert.ToInt32(args[0])];
            Encoding encoding = Encoding.GetEncoding(Convert.ToInt32(Properties.Settings.Default.encoding[Convert.ToInt32(args[0])]));

            outputFile = outputFile.Replace("[date]",DateTime.Now.ToString("yyyyMMddHHmm"));
            SqlConnection myConnection = new SqlConnection(connectionString);

            SqlParameter sOut1 = new SqlParameter("@declare", SqlDbType.VarChar,-1);
            sOut1.Value = "";
            sOut1.Direction = ParameterDirection.Output;
            SqlParameter sOut2 = new SqlParameter("@header", SqlDbType.VarChar,-1);
            sOut2.Value = "";
            sOut2.Direction = ParameterDirection.Output;
            SqlParameter sOut3 = new SqlParameter("@footer", SqlDbType.VarChar,-1);
            sOut3.Value = "";
            sOut3.Direction = ParameterDirection.Output;
            SqlParameter sOut4 = new SqlParameter("@TagToReplace", SqlDbType.VarChar, -1);
            sOut4.Value = "";
            sOut4.Direction = ParameterDirection.Output;
            SqlParameter sOut5 = new SqlParameter("@TagToReplaceWith", SqlDbType.VarChar, -1);
            sOut5.Value = "";
            sOut5.Direction = ParameterDirection.Output;
            try
            {
                myConnection.Open();
                using (System.Data.SqlClient.SqlCommand sqlCommand = myConnection.CreateCommand())
                {
                    

                    sqlCommand.CommandTimeout = 1800;
                    List<SqlParameter> lsp = new List<SqlParameter>();
                    int i = 0;
                    foreach (string p in parameters.Split(';'))
                    {
                        if (p != string.Empty)
                        {
                            SqlParameter sp = new SqlParameter("@" + i.ToString(), SqlDbType.VarChar);
                            sp.Value = p;
                            lsp.Add(sp);
                            i++;
                        }
                    }
                    foreach (SqlParameter sp in lsp)
                    {
                        sqlCommand.Parameters.Add(sp);
                    }
                    /* ALLEEN NODIG VOOR QUIZ */
                    sqlCommand.Parameters.Add(sOut1);
                    sqlCommand.Parameters.Add(sOut2);
                    sqlCommand.Parameters.Add(sOut3);
                    sqlCommand.Parameters.Add(sOut4);
                    sqlCommand.Parameters.Add(sOut5);
                    /**************************/
                    //SqlParameter returnParameter = sqlCommand.Parameters.Add("RetVal", SqlDbType.VarChar);
                    //returnParameter.Direction = ParameterDirection.ReturnValue;
                    

                    sqlCommand.CommandText = storedProc;
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    //sqlCommand.ExecuteScalar();
                    
                    using (XmlReader reader = sqlCommand.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = String.Empty;
                            
                            //s += sOut1.Value+Environment.NewLine;
                            //s += sOut2.Value.ToString();
                           
                            s += reader.ReadOuterXml();
                            s = sOut2.Value.ToString() +s;
                            s = "<?xml version=\"1.0\" encoding=\"" + encoding.WebName + "\"?>"+sOut1.Value + Environment.NewLine + s;
                            if(s.Contains("<bestandsNaam>"))
                            {
                                string sOutputFileName = s.Substring(s.IndexOf("<bestandsNaam>") + 14, s.IndexOf("</bestandsNaam>") - (s.IndexOf("<bestandsNaam>") + 14));
                                if (sOutputFileName.Trim().Length > 0)
                                {
                                    

                                    
                                    outputFile = sOutputFileName;
                                    
                                }
                            }
                            s += sOut3.Value;
                            s = s.Replace(sOut4.Value.ToString(), sOut5.Value.ToString());
                            using (FileStream fs = new FileStream(outputMap + "\\" + outputFile, FileMode.CreateNew))
                            {
                                using (StreamWriter writer = new StreamWriter(fs, encoding))
                                {
                                    writer.Write(s);
                                }
                            }

                            
                            // do something with s
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
